FROM golang:1.16-alpine AS src

# Cache module dependencies
COPY go.mod /go/src/app/go.mod
COPY go.sum /go/src/app/go.sum
WORKDIR /go/src/app/
RUN go mod download

COPY . ./

# Build Go Binary
RUN set -ex; \
    CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -o ./emergencycrewservice ./cmd/emergencycrewservice/main.go;

FROM alpine

# Add new user 'appuser'. App should be run without root privileges as a security measure
RUN adduser --home "/home/appuser" --disabled-password appuser --gecos "appuser,-,-,-"
USER appuser
RUN mkdir -p /home/appuser/app
WORKDIR /home/appuser/app/

COPY --from=src /go/src/app/emergencycrewservice .
COPY ./static ./static

EXPOSE 3333

# Run Go Binary
CMD /home/appuser/app/emergencycrewservice
