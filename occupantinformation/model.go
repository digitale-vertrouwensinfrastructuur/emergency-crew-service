// Licensed under the MIT license

package occupantinformation

import (
	"github.com/jinzhu/copier"

	"gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-service/utility/filter"
)

type OccupantInformationRequest struct {
	OccupantInformation
	filter.Filter
}

type OccupantInformationResponse struct {
	OccupantInformation
}

type Occupant struct {
	OccupantID            int64  `json:"occupant_id"`
	Name                  string `json:"name" validate:"required"`
	AdditionalInformation string `json:"additional_information"`
}

type HomeObject struct {
	HomeObjectID          int64  `json:"home_object_id"`
	Name                  string `json:"name" validate:"required"`
	AdditionalInformation string `json:"additional_information"`
}

type OccupantInformation struct {
	Occupants   []*Occupant   `json:"occupants"`
	HomeObjects []*HomeObject `json:"home_objects"`
}

func NewOccupantInformationResponse(occupantInformation *OccupantInformation) (OccupantInformationResponse, error) {
	var response OccupantInformationResponse

	err := copier.Copy(&response, &occupantInformation)
	if err != nil {
		return response, err
	}

	return response, nil
}

func NewOccupantInformationListResponse(occupantInformationList []*OccupantInformation) ([]OccupantInformationResponse, error) {
	var responses []OccupantInformationResponse

	for _, p := range occupantInformationList {
		var response OccupantInformationResponse

		err := copier.Copy(&response, &p)
		if err != nil {
			return responses, err
		}
		responses = append(responses, response)
	}

	return responses, nil
}
