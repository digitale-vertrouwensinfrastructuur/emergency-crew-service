// Licensed under the MIT license

package occupantinformation

import (
	"net/url"

	"gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-service/utility/filter"
)

type Filter struct {
	Base   filter.Filter
	Actor  string `json:"actor"`
	Action string `json:"action"`
	Goal   string `json:"goal"`
}

func GetFilters(queries url.Values) *Filter {
	f := filter.New(queries)
	return &Filter{
		Base:   *f,
		Actor:  queries.Get("actor"),
		Action: queries.Get("action"),
		Goal:   queries.Get("goal"),
	}
}
