// Licensed under the MIT license

package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"github.com/go-playground/validator/v10"

	"gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-service/occupantinformation"
	"gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-service/utility/respond"
	restclient "gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-service/utility/restclient"
)

type Handler struct {
	httpClient                         restclient.HTTPClient
	validate                           *validator.Validate
	occupantInformationServiceEndpoint string
}

func NewHandler(client restclient.HTTPClient, occupantInformationServiceEndpoint string) *Handler {
	return &Handler{
		httpClient:                         client,
		validate:                           validator.New(),
		occupantInformationServiceEndpoint: occupantInformationServiceEndpoint,
	}
}

// Get a occupantInformation by its address
// @Summary Get a OccupantInformation
// @Description Get a occupantInformation by its address.
// @Accept json
// @Produce json
// @Success 200 {object} models.OccupantInformation
// @Router /api/v1/occupant-information/{occupantInformationID} [get]
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	address := r.FormValue("address")
	if address == "" {
		log.Println("Address provided is empty")
		respond.Error(w, http.StatusBadRequest, nil)
		return
	}

	oi, err := fetchOccupantInformation(address, h.httpClient, h.occupantInformationServiceEndpoint)
	if err != nil {
		log.Println(fmt.Sprintf("Error occurred while trying to fetch occupant information on address '%s'", address))
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte(err.Error()))
		if err != nil {
			log.Println(err)
		}
		return
	}

	respond.Render(w, http.StatusOK, occupantinformation.OccupantInformationResponse{OccupantInformation: *oi})
}

func fetchOccupantInformation(address string, client restclient.HTTPClient, occupantInformationServiceEndpoint string) (*occupantinformation.OccupantInformation, error) {
	occupantInformationService, _ := url.Parse(occupantInformationServiceEndpoint)
	params := url.Values{}
	params.Add("address", address)
	occupantInformationService.RawQuery = params.Encode()

	log.Printf("Fetching occupant information on: %s", occupantInformationService.String())
	response, err := client.Get(occupantInformationService.String())
	if err != nil {
		log.Println("Error occurred while fetching occupant information")
		return nil, err
	}

	defer response.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(response.Body)
	if response.StatusCode != 200 {
		return nil, fmt.Errorf("unable to fetch OccupantInformation: %s", bodyBytes)
	}

	var oi occupantinformation.OccupantInformation
	err = json.Unmarshal(bodyBytes, &oi)
	if err != nil {
		return nil, err
	}

	return &oi, nil
}
