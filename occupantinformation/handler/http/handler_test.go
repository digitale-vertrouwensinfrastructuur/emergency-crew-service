// Licensed under the MIT license

package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	chi "github.com/go-chi/chi/v5"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-service/occupantinformation"
	"gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-service/utility/mock"
)

//go:generate mockgen -package mock -source ../../handler.go -destination=../../mock/mock_handler.go

func TestHandler_Get(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	address := "Duckstad, Kweklaan 36"
	clientRespBody := occupantinformation.OccupantInformation{
		Occupants: []*occupantinformation.Occupant{
			{
				OccupantID:            66,
				Name:                  "Kwek",
				AdditionalInformation: "Can talk alot",
			},
		},
		HomeObjects: []*occupantinformation.HomeObject{
			{
				HomeObjectID:          42,
				Name:                  "Gas Cannister",
				AdditionalInformation: "In the hallway",
			},
		},
	}
	clientRespJson, err := json.Marshal(clientRespBody)
	assert.NoError(t, err)
	clientResponseBody := ioutil.NopCloser(bytes.NewReader(clientRespJson))
	clientResponse := http.Response{
		StatusCode: 200,
		Body:       clientResponseBody,
	}

	client := mock.NewMockHTTPClient(ctrl)
	requestUrl := fmt.Sprintf("http://localhost:3082/api/v1/occupant-information?address=%s", url.QueryEscape(address))
	client.EXPECT().Get(requestUrl).Return(&clientResponse, nil).AnyTimes()

	router := chi.NewRouter()
	RegisterHTTPEndPoints(router, client, "http://localhost:3082/api/v1/occupant-information")

	res := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("/api/v1/occupant-information/?address=%s", address), nil)
	assert.NoError(t, err)

	router.ServeHTTP(res, req)
	body, err := ioutil.ReadAll(res.Result().Body)
	assert.NoError(t, err)

	var gotOccupantInformation occupantinformation.OccupantInformationResponse
	err = json.Unmarshal(body, &gotOccupantInformation)
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, clientRespBody.HomeObjects[0].HomeObjectID, gotOccupantInformation.HomeObjects[0].HomeObjectID)
	assert.Equal(t, clientRespBody.HomeObjects[0].Name, gotOccupantInformation.HomeObjects[0].Name)
	assert.Equal(t, clientRespBody.HomeObjects[0].AdditionalInformation, gotOccupantInformation.HomeObjects[0].AdditionalInformation)
	assert.Equal(t, clientRespBody.Occupants[0].OccupantID, gotOccupantInformation.Occupants[0].OccupantID)
	assert.Equal(t, clientRespBody.Occupants[0].Name, gotOccupantInformation.Occupants[0].Name)
	assert.Equal(t, clientRespBody.Occupants[0].AdditionalInformation, gotOccupantInformation.Occupants[0].AdditionalInformation)
}
