// Licensed under the MIT license

package http

import (
	"github.com/go-chi/chi/v5"

	"gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-service/middleware"
	"gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-service/utility/restclient"
)

func RegisterHTTPEndPoints(router *chi.Mux, client restclient.HTTPClient, occupantInformationServiceEndpoint string) {
	h := NewHandler(client, occupantInformationServiceEndpoint)

	router.Route("/api/v1/occupant-information", func(router chi.Router) {
		router.Use(middleware.Json)
		router.Get("/", h.Get)
	})
}
