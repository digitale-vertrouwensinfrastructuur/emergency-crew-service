# Emergency crew Service
The emergency crew service is responsible for providing relevant information to emergency crews during emergencies.

## Requirements
- go 1.16+

## Building
### Build using go
```
go build ./...
```
### Build using Task
Install Task according to [Task installation](https://taskfile.dev/#/installation) and then run:
```
task build
```
More Task command can be shown using `task list`
## Running locally
### Run using go
```
go run ./cmd/emergencycrewservice.go
```
### Run using Skaffold
Install microk8s according to [Microk8s installation](https://microk8s.io/docs/install-alternatives)
Install Helm according to [Helm installation](https://helm.sh/docs/intro/install/)
Install Skaffold according to [Skaffold installation](https://skaffold.dev/docs/install/)
Copy the values from `microk8s.kubectl config view` to $HOME/.kube/config

```bash
docker login registry.gitlab.com # a gitlab access token can be used as password here

kubectl config use-context microk8s
kubectl create ns dvi-fire-brigade
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=</absolute/path/to/.docker/config.json> \
    --type=kubernetes.io/dockerconfigjson \
    --namespace=dvi-fire-brigade
```
This `regcred` secret will be used by the Helm deployment to fetch the Docker image. Make sure to create it in the correct namespace.

Alternatively, it is also possible to create the regcred using:
```
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<gitlab-access-token> --docker-email=<email> --namespace=dvi-fire-brigade
```

Then run:

```bash
skaffold dev --cleanup=false -p fire-brigade
```

It may take some time for all resources to become healthy.

To test that the emergency-crew-api is up, make sure you can connect to it, for example using a port-forward

```bash
kubectl port-forward service/emergency-crew-service 8080:http --namespace=dvi-fire-brigade
curl localhost:8080/health
```
### Run using Docker-compose
```
docker-compose up
```
## Testing
### Testing using Task
```
go test ./...
```
### Testing using Task
```
task test
```
### Testing REST endpoints manually
The `examples/occupants.http` file can be used to test the REST endpoint

### Test using the UI
A UI is provided on the path `/ui`. By default this is available on `http://localhost:3333/ui`

### Configuration
The application uses the following environment variables:

| Environment variable                           | Type          | Default                                           | Description                                                   |
| :--------------------------------------------- | :-----------: | :-----------------------------------------------: | ------------------------------------------------------------: |
| API_NAME                                       | string        | emergency-crew                                    | Name of the API                                               |
| API_HOST                                       | string        | 0.0.0.0                                           | Host on which the API will be available                       |
| API_PORT                                       | string        | 3333                                              | Port on which the API will be available                       |
| API_VERSION                                    | string        | v0.1.0                                            | Version of this API                                           |
| API_READ_TIMEOUT                               | time.Duration | 5s                                                | Time allowed to read entire request including body            |
| API_READ_HEADER_TIMEOUT                        | time.Duration | 5s                                                | Time allowed to read request headers                          |
| API_WRITE_TIMEOUT                              | time.Duration | 10s                                               | Time allowed to write the response                            |
| API_IDLE_TIMEOUT                               | time.Duration | 120s                                              | Time allowed for slow running tasks to finish during shutdown |
| API_REQUEST_LOG                                | bool          | true                                              | Whether requests should be logged                             |
| SERVICES_HTTP_CLIENT_TIMEOUT                   | time.Duration | 30s                                               | Time allowed to wait for HTTP client service calls            |
| SERVICES_OCCUPANT_INFORMATION_SERVICE_ENDPOINT | string        | http://localhost:3082/api/v1/occupant-information | Endpoint of the occupant information service                  |
| FILE_SERVER_UI_DIR                             | string        | $workdir/../../ui                                 | Directory from which UI will be served                        |

## License
See [LICENSE.md](LICENSE.md)
