async function getOccupantInformation() {
    let adress = document.getElementById("address_selector").value
    let url = "/api/v1/occupant-information/?address="+adress;
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
        return false;
    }
}

async function renderOccupantInformation() {
    let occupantInformation = await getOccupantInformation();
    console.log(occupantInformation)
    let htmlSegment;
    if (typeof(occupantInformation) === 'object' && (occupantInformation.occupants || occupantInformation.home_objects)) {
        htmlSegment = `<div class="occupantInformation">
        <span>Bewoners:</span><ul>`;
        occupantInformation.occupants.forEach(occupant => {
            htmlSegment += `<li>${occupant.name}: <i>${occupant.additional_information}</i></li>`;
        });

        htmlSegment += `</ul><span>Objecten in huis:</span><ul>`;
        occupantInformation.home_objects.forEach(homeObject => {
            htmlSegment += `<li>${homeObject.name}: <i>${homeObject.additional_information}</i></li>`;
        });
        htmlSegment += `</ul></div>`;
    } else {
        htmlSegment = `<div class="occupantInformation">
                    <span>Resultaat: Geen toegang tot bewonersinformatie </span><br/>
                </div>`;
    }

    let container = document.querySelector('.occupantInformationList');
    container.innerHTML = htmlSegment;
}

async function enableSubmitButton() {
    let button = document.querySelector('#submit_button');
    console.log(button);
    button.disabled = false;
}
