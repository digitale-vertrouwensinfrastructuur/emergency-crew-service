/* Licensed under the MIT license */

package restclient

import (
	"net/http"
)

type HTTPClient interface {
	Get(url string) (*http.Response, error)
}
