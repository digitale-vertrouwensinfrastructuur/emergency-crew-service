// Licensed under the MIT license

package server

import (
	"net/http"

	occupantInformationHandler "gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-service/occupantinformation/handler/http"
)

func (s *Server) initOccupantInformation() {
	s.initOccupantInformationList()
}

func (s *Server) initOccupantInformationList() {
	client := &http.Client{
		Timeout: s.cfg.Services.HttpClientTimeout,
	}

	occupantInformationHandler.RegisterHTTPEndPoints(s.router, client, s.cfg.Services.OccupantInformationServiceEndpoint)
}
